package br.com.inmetrics.bancodemassa.api;

import static br.com.inmetrics.bancodemassa.api.utils.ContentTypeUtils.ETL;
import static br.com.inmetrics.bancodemassa.api.utils.JsonUtils.getTags;
import static br.com.inmetrics.bancodemassa.api.utils.JsonUtils.hasValue;
import static br.com.inmetrics.bancodemassa.api.utils.JsonUtils.isPost;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import com.google.gson.JsonObject;

import br.com.inmetrics.bancodemassa.api.providers.Cluster;
import br.com.inmetrics.bancodemassa.api.providers.DataGet;
import br.com.inmetrics.bancodemassa.api.providers.DataPost;
import br.com.inmetrics.bancodemassa.api.providers.DataPut;
import br.com.inmetrics.bancodemassa.api.providers.Node;
import br.com.inmetrics.bancodemassa.api.providers.Status;
import br.com.inmetrics.bancodemassa.api.providers.config.Tag;
import br.com.inmetrics.bancodemassa.api.service.NodeService;
import br.com.inmetrics.bancodemassa.api.service.imp.NodeServiceImp;

public class ApiTest {

	@Test
	public void deveTrazerUmaMassaDadoComoParametroApenasUmaClasseDeMassa() {
		Node node = new Node(new Cluster(ETL.getId())).buscarMassaDeTeste(new DataGet("ETL.EC"));
		JsonObject resultado = new NodeServiceImp().buscar(node);
		assertNotNull(resultado);
	}

	@Test
	public void deveTrazerUmaMassaDadoComoParametroUmaClasseDeMassaECodigoDeReserva() {
		
		Node node = new Node(new Cluster(ETL.getId()))
				.buscarMassaDeTeste(new DataGet("ETL.EC")
				.adicionarConfiguracaoDoCodigoDaReserva("RESERVA.NAO-ALTERA-STATUS-EC"));
		
		JsonObject resultado = new NodeServiceImp().buscar(node);
		assertNotNull(resultado);
	}

	@Test
	public void deveRealizarUmPostNoCantina() {

		ClasseMassaDemo classeMassaDemo = new ClasseMassaDemo();
		classeMassaDemo.setBanco("237");
		classeMassaDemo.setAgencia("6516");
		classeMassaDemo.setConta("000284044");
		classeMassaDemo.setUltimoDigitoDaConta("8");
		classeMassaDemo.setTipoDaConta("1");
		
		Node node = new Node(new Cluster(ETL.getId()))
				.incluirMassaDeTeste(new DataPost(new Tag("PIPELINE"))
				.incluir(classeMassaDemo));

		JsonObject resultado = new NodeServiceImp().incluir(node);
		assertNotNull(resultado);

	}

	@Test
	public void deveRealizarUmPutNoCantina() {

		NodeService nodeService = new NodeServiceImp();

		ClasseMassaDemo classeMassaDemo = new ClasseMassaDemo();
		classeMassaDemo.setBanco("237");
		classeMassaDemo.setAgencia("6516");
		classeMassaDemo.setConta("000284044");
		classeMassaDemo.setUltimoDigitoDaConta("8");
		classeMassaDemo.setTipoDaConta("1");

		Node node = new Node(new Cluster(ETL.getId()))
				.incluirMassaDeTeste(new DataPost(new Tag("PIPELINE.JUNITTEST"))
				.incluir(classeMassaDemo));

		JsonObject resultadoDaInclusao = nodeService.incluir(node);

		if (isPost(resultadoDaInclusao)) {

			node.buscarMassaDeTeste(new DataGet("PIPELINE.JUNITTEST"));
			JsonObject resultadoDaConsulta = nodeService.buscar(node);
			
			if (hasValue(resultadoDaConsulta)) {
				
				String classeDaMassa = getTags(resultadoDaConsulta).get("classe_massa").getAsString();
				String id = resultadoDaConsulta.getAsJsonObject("get_return").get("id").getAsString();
				String md5 = resultadoDaConsulta.getAsJsonObject("get_return").get("md5").getAsString();
				
				node.alterarStatusDaMassaDeTeste(new DataPut(classeDaMassa, id, md5).alterar(new Status(null, "false")));
				JsonObject resultadoDaAlteracao = nodeService.alterarStatusDaMassa(node);
				assertNotNull(resultadoDaAlteracao);
			}
		}
	}
}
