package br.com.inmetrics.bancodemassa.api;

import br.com.inmetrics.bancodemassa.api.utils.MappingFromGherkin;

public class ClasseMassaDemo {
	
	@MappingFromGherkin(key = "cd_bank", name = "Banco")
	private String banco = null;
	
	@MappingFromGherkin(key="nu_agency", name = "Agencia")
	private String agencia = null;
	
	@MappingFromGherkin(key="nu_customer", name = "Numero do Cliente")
	private String numeroDoCliente = null;
	
	@MappingFromGherkin(key="nu_current_account", name = "Conta")
	private String conta = null;
	
	@MappingFromGherkin(key="cd_merchant_bank_type", name = "Tipo da Conta")
	private String tipoDaConta = null;
	
	@MappingFromGherkin(key="cd_product", name = "Codigo do Produto")
	private String codigoDoProduto = null;
	
	@MappingFromGherkin(key="dc_payment_method", name = "Forma de Pagamento")
	private String formaDePagamento = null;
	
	@MappingFromGherkin(key="nm_card_association", name = "Bandeira")
	private String bandeira = null;
	
	@MappingFromGherkin(key="dc_payment_category", name = "Tipo de Pagamento")
	private String tipoDePagamento = null;
	
	@MappingFromGherkin(key="nu_digit_control_agency", name = "Ultimo digito da agencia")
	private String ultimoDigitoDaAgencia = null;
	
	@MappingFromGherkin(key="nu_digit_control_account", name = "Ultimo digito da conta")
	private String ultimoDigitoDaConta = null;
	
	@MappingFromGherkin(key="nu_hierarchical_node", name = "")
	private String nuHierarchicalNode = null;
	
	@MappingFromGherkin(key="cd_contract_card_assoc", name = "")
	private String cdContractCardAssoc = null;
	
	private String gerarContaCorrente = null;

	public String getBanco() {
		return banco;
	}

	public String getAgencia() {
		return agencia;
	}

	public String getConta() {
		return conta;
	}

	public String getTipoDaConta() {
		return tipoDaConta;
	}

	public String getCodigoDoProduto() {
		return codigoDoProduto;
	}

	public String getFormaDePagamento() {
		return formaDePagamento;
	}

	public String getBandeira() {
		return bandeira;
	}

	public String getTipoDePagamento() {
		return tipoDePagamento;
	}

	public String getUltimoDigitoDaAgencia() {
		return ultimoDigitoDaAgencia;
	}

	public String getUltimoDigitoDaConta() {
		return ultimoDigitoDaConta;
	}

	public String getNuHierarchicalNode() {
		return nuHierarchicalNode;
	}

	public String getCdContractCardAssoc() {
		return cdContractCardAssoc;
	}

	public String getGerarContaCorrente() {
		return gerarContaCorrente;
	}

	public void setGerarContaCorrente(String gerarContaCorrente) {
		this.gerarContaCorrente = gerarContaCorrente;
	}

	public String getNumeroDoCliente() {
		return numeroDoCliente;
	}

	public void setNumeroDoCliente(String numeroDoCliente) {
		this.numeroDoCliente = numeroDoCliente;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public void setTipoDaConta(String tipoDaConta) {
		this.tipoDaConta = tipoDaConta;
	}

	public void setCodigoDoProduto(String codigoDoProduto) {
		this.codigoDoProduto = codigoDoProduto;
	}

	public void setFormaDePagamento(String formaDePagamento) {
		this.formaDePagamento = formaDePagamento;
	}

	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}

	public void setTipoDePagamento(String tipoDePagamento) {
		this.tipoDePagamento = tipoDePagamento;
	}

	public void setUltimoDigitoDaAgencia(String ultimoDigitoDaAgencia) {
		this.ultimoDigitoDaAgencia = ultimoDigitoDaAgencia;
	}

	public void setUltimoDigitoDaConta(String ultimoDigitoDaConta) {
		this.ultimoDigitoDaConta = ultimoDigitoDaConta;
	}

	public void setNuHierarchicalNode(String nuHierarchicalNode) {
		this.nuHierarchicalNode = nuHierarchicalNode;
	}

	public void setCdContractCardAssoc(String cdContractCardAssoc) {
		this.cdContractCardAssoc = cdContractCardAssoc;
	}
	
}
