package br.com.inmetrics.bancodemassa.api.providers;

import static java.util.Objects.requireNonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static br.com.inmetrics.bancodemassa.api.utils.EnvironmentTypeUtils.*;

public class Cluster {
	
	@Expose
	@SerializedName(value = "environment_type")
	private String environmentType = null;
	
	@Expose
	@SerializedName(value = "content_type")
	private String contentType = null;
	
	public Cluster() {
	}
	
	public Cluster(String tipoDeMassa) {
		this.environmentType = HML.getId();
		this.contentType = tipoDeMassa;
		validateValue();
	}

	public void validateValue() {
		requireNonNull(this.contentType, "Necessario informar pelo menos um tipo de 'contentType' para a massa");
	}

	public String getAmbiente() {
		return environmentType;
	}
	
	public String getTipoDeMassa() {
		return contentType;
	}
	
	@Override
	public String toString() {
		return "[Enviroment-Type: " + environmentType + ", Content-Type: " + contentType + "].";
	}

}
