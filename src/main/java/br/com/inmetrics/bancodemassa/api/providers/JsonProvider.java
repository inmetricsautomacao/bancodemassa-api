package br.com.inmetrics.bancodemassa.api.providers;

public interface JsonProvider {

	String getJsonDoCluster();
	
	String getJson();
	
	String postJson();
	
	String putJson();
	
}
