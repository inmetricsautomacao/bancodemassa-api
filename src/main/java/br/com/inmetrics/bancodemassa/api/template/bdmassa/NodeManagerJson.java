package br.com.inmetrics.bancodemassa.api.template.bdmassa;

import static br.com.inmetrics.bancodemassa.api.utils.JsonUtils.getGson;
import static br.com.inmetrics.bancodemassa.api.utils.JsonUtils.parserStringTOJson;
import static br.com.inmetrics.bancodemassa.api.utils.StringUtils.formataJsonGet;
import static br.com.inmetrics.bancodemassa.api.utils.StringUtils.formataJsonPost;
import static br.com.inmetrics.bancodemassa.api.utils.StringUtils.formataJsonPut;

import br.com.inmetrics.bancodemassa.api.providers.Node;
import br.com.inmetrics.bancodemassa.api.template.JsonTemplate;

/**
 * Esta classe consiste exclusivamente de um gerenciador de templates Json para
 * uma determinada operação 'CRUD'.
 * 
 * <p>
 * <strong>Gerenciador de Templates apenas para Node</strong>
 * </p>
 *
 * @author Thiago Sakurai Paschoal
 * 
 * @see Object
 * @see JsonTemplate
 * 
 * @since 1.0
 * 
 */
public class NodeManagerJson implements JsonTemplate {

	private Node node = null;

	public NodeManagerJson(Node input) {
		this.node = input;
	}

	@Override
	public String getJson() {
		return formataJsonGet(parserStringTOJson(getGson().toJson(node)).toString());
	}

	@Override
	public String postJson() {
		return formataJsonPost(parserStringTOJson(getGson().toJson(node)).toString());
	}

	@Override
	public String putJson() {
		return formataJsonPut(parserStringTOJson(getGson().toJson(node)).toString());
	}
	
}
