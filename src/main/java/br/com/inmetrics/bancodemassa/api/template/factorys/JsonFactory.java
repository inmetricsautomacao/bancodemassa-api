package br.com.inmetrics.bancodemassa.api.template.factorys;

import br.com.inmetrics.bancodemassa.api.providers.Node;
import br.com.inmetrics.bancodemassa.api.template.JsonTemplate;

/**
 * Factory para criação de novas Templates JSON
 *
 * @author  Thiago Sakurai Paschoal
 * 
 * @see     Object
 * 
 * @see		JsonFactory
 * 
 * @since   1.0
 * 
 */
public interface JsonFactory {
	
	JsonTemplate createJson(Node nodeInput);
	
}
