package br.com.inmetrics.bancodemassa.api.template.bdmassa;

import static br.com.inmetrics.bancodemassa.api.utils.JsonUtils.getGson;
import static br.com.inmetrics.bancodemassa.api.utils.JsonUtils.parserStringTOJson;

import br.com.inmetrics.bancodemassa.api.providers.Cluster;
import br.com.inmetrics.bancodemassa.api.providers.DataGet;
import br.com.inmetrics.bancodemassa.api.providers.Node;
import br.com.inmetrics.bancodemassa.api.providers.ReuseControl;
import br.com.inmetrics.bancodemassa.api.template.JsonTemplate;

/**
 * Esta classe consiste exclusivamente de um gerenciador de templates Json para uma determinada operação 'CRUD'.
 * 
 * <p><strong>Gerenciador de Templates apenas para Cluster</strong></p>
 *
 * @author  Thiago Sakurai Paschoal
 * 
 * @see     Object
 * @see     JsonTemplate
 * 
 * @since   1.0
 * 
 */
public class ClusterManagerJson implements JsonTemplate {
	
	private Node input = null;
	
	public ClusterManagerJson(Node input) {
		this.input = input;
	}

	@Override
	public String getJson() {
		return getJsonClusterTemplate();
	}
	
	@Override
	public String postJson() {
		return null;
	}

	@Override
	public String putJson() {
		return null;
	}
	
	private String getJsonClusterTemplate() {
		final int controleDeReuso = 0;
		if (input.getConfiguracoesParaIncluirUmaMassa() != null) {
			input = new Node(new Cluster(input.getCluster().getTipoDeMassa())).buscarMassaDeTeste(new DataGet(input.getConfiguracoesParaIncluirUmaMassa().getClasseDeMassa()));
		}
		input.adicionarControleDeReuso(new ReuseControl(controleDeReuso));
		return parserStringTOJson(getGson().toJson(input)).toString();
	}

}
