package br.com.inmetrics.bancodemassa.api.service;

import com.google.gson.JsonObject;

import br.com.inmetrics.bancodemassa.api.providers.Node;

/**
 * Interface de gerenciamento dos nodes (Service)
 * 
 * @author ThiagoSakurai
 *
 * @see Object
 * 
 * @since 1.0.0
 * 
 */
public interface NodeService {
	
	public JsonObject buscar(Node node);
	
	public JsonObject incluir(Node node);
	
	public JsonObject alterarStatusDaMassa(Node node);
	
}
