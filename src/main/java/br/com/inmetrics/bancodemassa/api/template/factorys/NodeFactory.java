package br.com.inmetrics.bancodemassa.api.template.factorys;

import br.com.inmetrics.bancodemassa.api.providers.Node;
import br.com.inmetrics.bancodemassa.api.template.JsonTemplate;
import br.com.inmetrics.bancodemassa.api.template.bdmassa.NodeManagerJson;

/**
 * Factory para criação apenas de um novo Gerenciador de Templates de Node.
 *
 * @author  Thiago Sakurai Paschoal
 * 
 * @see     Object
 * 
 * @see		JsonFactory
 * 
 * @since   1.0
 * 
 */
public class NodeFactory implements JsonFactory {

	@Override
	public JsonTemplate createJson(Node nodeInput) {
		return new NodeManagerJson(nodeInput);
	}

}
