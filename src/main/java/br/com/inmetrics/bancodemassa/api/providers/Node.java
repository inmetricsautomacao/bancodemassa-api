package br.com.inmetrics.bancodemassa.api.providers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.inmetrics.bancodemassa.api.template.JsonTemplate;
import br.com.inmetrics.bancodemassa.api.template.factorys.ClusterFactory;
import br.com.inmetrics.bancodemassa.api.template.factorys.JsonFactory;
import br.com.inmetrics.bancodemassa.api.template.factorys.NodeFactory;

public class Node implements JsonProvider {
	
	@Expose
	@SerializedName(value = "node")
	private Cluster cluster = null;
	
	@Expose
	@SerializedName(value = "get")
	private DataGet configurationToGetData = null;
	
	@Expose
	@SerializedName(value = "post")
	private DataPost configurationToIncludeData = null;
	
	@Expose
	@SerializedName(value = "put")
	private DataPut configurationToChangeStatusData = null;
	
	@Expose
	@SerializedName(value = "controle")
	private ReuseControl controleDeReuso = null;
	
	public Node(Cluster cluster) {
		this.cluster = cluster;
	}
	
	public String getJsonDoCluster() {
		JsonFactory jsonFactory = new ClusterFactory();
		JsonTemplate jsonTemplate = jsonFactory.createJson(this);
		return jsonTemplate.getJson();
	}
	
	public String getJson() {
		JsonFactory jsonFactory = new NodeFactory();
		JsonTemplate jsonTemplate = jsonFactory.createJson(this);
		return jsonTemplate.getJson();
	}
	
	public String postJson() {
		JsonFactory jsonFactory = new NodeFactory();
		JsonTemplate jsonTemplate = jsonFactory.createJson(this);
		return jsonTemplate.postJson();
	}
	
	public String putJson() {
		JsonFactory jsonFactory = new NodeFactory();
		JsonTemplate jsonTemplate = jsonFactory.createJson(this);
		return jsonTemplate.putJson();
	}
	
	public Node adicionarControleDeReuso(ReuseControl controleDeReuso) {
		this.controleDeReuso = controleDeReuso;
		return this;
	}
	
	public Node buscarMassaDeTeste(DataGet pedidoDeMassa) {
		this.configurationToGetData = pedidoDeMassa;
		this.configurationToIncludeData = null;
		this.configurationToChangeStatusData = null;
		this.controleDeReuso = null;
		return this;
	}
	
	public Node incluirMassaDeTeste(DataPost massaParaIncluir) {
		this.configurationToIncludeData = massaParaIncluir;
		this.configurationToGetData = null;
		this.configurationToChangeStatusData = null;
		this.controleDeReuso = null;
		return this;
	}
	
	public Node alterarStatusDaMassaDeTeste(DataPut massaParaAlterar) {
		this.configurationToChangeStatusData = massaParaAlterar;
		this.configurationToGetData = null;
		this.configurationToIncludeData = null;
		this.controleDeReuso = null;
		return this;
	}
	
	public ReuseControl getControleDeReuso() {
		return controleDeReuso;
	}

	public DataGet getConfiguracoesParaBuscarUmaMassa() {
		return configurationToGetData;
	}

	public DataPost getConfiguracoesParaIncluirUmaMassa() {
		return configurationToIncludeData;
	}

	public DataPut getConfiguracoesParaAlterarStatusDeUmaMassa() {
		return configurationToChangeStatusData;
	}
	
	public Cluster getCluster() {
		return cluster;
	}

}
