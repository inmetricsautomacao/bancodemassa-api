package br.com.inmetrics.bancodemassa.api.providers;

import static br.com.inmetrics.bancodemassa.api.utils.StringUtils.isNotNull;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataPut {
	
	@Expose
	@SerializedName(value = "classe_massa")
	private String groupMass = null;
	
	@Expose
	@SerializedName(value = "id")
	private String id = null;
	
	@Expose
	@SerializedName(value = "md5")
	private String hash = null;
	
	@Expose
	@SerializedName(value = "update")
	private List<Status> status = null;

	public DataPut(String groupMass, String id, String hash) {
		if (isNotNull(id) && isNotNull(hash)) {
			this.groupMass = groupMass;
			this.id = id;
			this.hash = hash;
		} else {
			throw new NullPointerException("Necessario informar pelo menos um id e um hash md5 para 'PUT'");
		}
	}
	
	public DataPut alterar(Status update) {
		this.status = new ArrayList<>();
		this.status.add(update);
		return this;
	}

	public String getGroupMass() {
		return groupMass;
	}

	public String getId() {
		return id;
	}

	public String getHash() {
		return hash;
	}

	public List<Status> getUpdates() {
		return status;
	}

}
