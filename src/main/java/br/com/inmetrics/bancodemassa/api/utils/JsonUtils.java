package br.com.inmetrics.bancodemassa.api.utils;

import static java.sql.Types.BIGINT;
import static java.sql.Types.BINARY;
import static java.sql.Types.BIT;
import static java.sql.Types.CHAR;
import static java.sql.Types.DATALINK;
import static java.sql.Types.DATE;
import static java.sql.Types.DECIMAL;
import static java.sql.Types.DOUBLE;
import static java.sql.Types.FLOAT;
import static java.sql.Types.INTEGER;
import static java.sql.Types.LONGNVARCHAR;
import static java.sql.Types.LONGVARCHAR;
import static java.sql.Types.NCHAR;
import static java.sql.Types.NULL;
import static java.sql.Types.NUMERIC;
import static java.sql.Types.NVARCHAR;
import static java.sql.Types.OTHER;
import static java.sql.Types.ROWID;
import static java.sql.Types.SMALLINT;
import static java.sql.Types.TIME;
import static java.sql.Types.TIMESTAMP;
import static java.sql.Types.TINYINT;
import static java.sql.Types.VARBINARY;
import static java.sql.Types.VARCHAR;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Classe com metodos utilitarios para manipulacoes de {@link JsonObject}
 * 
 * <p>
 * JsonUtils fornece:
 * <ul>
 * <li>Metodos para obter um {@link JsonObject} a partir de um
 * {@link ResultSetMetaData}</li>
 * </ul>
 * 
 * @author Inmetrics
 *
 */
public final class JsonUtils {

	/**
	 * Construtor privado
	 */
	private JsonUtils() {
	}

	/**
	 * Retorna o tipo do objeto {@link JsonObject}
	 * 
	 * @param resultSet
	 * @param rsmd
	 * @param jsonObjRow
	 * @return
	 * @throws SQLException
	 */
	public static JsonObject getJsonObjRow(ResultSet resultSet, ResultSetMetaData rsmd, JsonObject jsonObjRow)
			throws SQLException {

		for (int i = 1; i <= rsmd.getColumnCount(); i++) {
			switch (rsmd.getColumnType(i)) {
			case BIGINT:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getLong(i));
				break;
			case BINARY:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getShort(i));
				break;
			case BIT:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getShort(i));
				break;
			case INTEGER:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getInt(i));
				break;
			case ROWID:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case SMALLINT:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getInt(i));
				break;
			case TINYINT:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getInt(i));
				break;
			case DECIMAL:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getDouble(i));
				break;
			case DOUBLE:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getDouble(i));
				break;
			case FLOAT:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getDouble(i));
				break;
			case NUMERIC:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getBigDecimal(i));
				break;
			case DATE:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case DATALINK:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case TIMESTAMP:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case TIME:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case CHAR:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case LONGNVARCHAR:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case LONGVARCHAR:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case NCHAR:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case NULL:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case NVARCHAR:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case OTHER:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case VARBINARY:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			case VARCHAR:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			default:
				jsonObjRow.addProperty(rsmd.getColumnLabel(i).toLowerCase(), resultSet.getString(i));
				break;
			}
		}
		return jsonObjRow;
	}

	/**
	 * Get Json
	 * 
	 * @return
	 */
	public static Gson getGson() {
		return new GsonBuilder().disableHtmlEscaping().excludeFieldsWithoutExposeAnnotation().setPrettyPrinting()
				.create();
	}

	/**
	 * Retorna um objeto {@link JSONObject} contendo apenas o ne da massa
	 * 
	 * @param resultado
	 *            - Json retornado pelo Cantina
	 * 
	 * @return {@link JsonObject} ne de massa
	 * 
	 * @see JsonObject
	 * 
	 */
	public static JsonObject getMassa(JsonObject resultado) {
		return resultado.getAsJsonObject("get_return").getAsJsonObject("data").getAsJsonObject("massa");
	}

	/**
	 * Retorna um objeto {@link JSONObject} contendo apenas o ne de tags
	 * 
	 * @param resultado
	 *            - Json retornado pelo Cantina
	 * 
	 * @return {@link JsonObject} ne de massa
	 * 
	 * @see JsonObject
	 * 
	 */
	public static JsonObject getTags(JsonObject resultado) {
		return resultado.getAsJsonObject("get_return").getAsJsonObject("data").getAsJsonObject("tags");
	}

	/**
	 * Retorna 'true' or 'false' caso o status_code tenha incluedo a massa com
	 * sucesso
	 * 
	 * @param Json
	 *            retornado pelo Cantina
	 * 
	 * @return 'true' or 'false'
	 * 
	 */
	public static boolean hasValue(JsonObject jsonResult) {
		return jsonResult.getAsJsonObject("get_return").get("status_code").getAsInt() == 1 ? true : false;
	}

	/**
	 * Retorna 'true' or 'false' caso o status_code traga registros ou neo
	 * 
	 * @param Json
	 *            retornado pelo Cantina
	 * 
	 * @return 'true' or 'false'
	 * 
	 */
	public static boolean isPost(JsonObject jsonResult) {
		return jsonResult.getAsJsonObject("post_return").get("status_code").getAsInt() == 1 ? true : false;
	}

	/**
	 * Retorna 'true' or 'false' caso o status_code traga registros ou neo
	 * 
	 * @param Json
	 *            retornado pelo Cantina
	 * 
	 * @return 'true' or 'false'
	 * 
	 */
	public static boolean isPut(JsonObject jsonResult) {
		return jsonResult.getAsJsonObject("put_return").get("status_code").getAsInt() == 1 ? true : false;
	}

	public static JsonElement getJsonElement(JsonObject result) {
		return result.get("resultset");
	}

	/**
	 * Converte objeto 'String' para 'JSON'
	 * 
	 * @param json
	 * 
	 * @see JsonObject
	 * 
	 * @return {@link JSONObject}
	 * 
	 */
	public static JsonObject parserStringTOJson(String json) {
		return new JsonParser().parse(json).getAsJsonObject();
	}

	/**
	 * Converte objeto 'Json' para 'LocalDate'
	 * 
	 * @param json
	 * 
	 * @see JsonObject
	 * 
	 * @return {@link JSONObject}
	 * 
	 */
	public static LocalDate convertJsonToLocalDate(JsonObject json) {
		return LocalDate.parse(json.getAsJsonObject("get_return").get("datahora_carga").getAsString().substring(0, 10), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
	
	/**
	 * Retorna um json da com as colunas.
	 * @param jsonArray
	 * @param collumn
	 * @author IcaroSilva
	 * @return
	 */
	public static String getJsonValue(JsonArray jsonArray, String collumn) {
		for(JsonElement json : jsonArray) {
			return getJsonValue(json, collumn);
		}	
		return null;
	}
	
	/**
	 * Retorna um json da com as colunas.
	 * @param jsonArray
	 * @param collumn
	 * @author IcaroSilva
	 * @return
	 */
	public static List<String> getListValue(JsonArray jsonArray, String collumn) {
		List<String> jsons = new ArrayList<>();
		for(JsonElement json : jsonArray) {
			jsons.add(getJsonValue(json, collumn));
		}	
		return jsons;
	}
	
	/**
	 * Retorna uma string json
	 * @param json
	 * @param collumn
	 * @author IcaroSilva
	 * @return
	 */
	public static String getJsonValue(JsonElement json, String collumn) {
		String value = null;
		collumn = collumn.toLowerCase();
		if((null != json.getAsJsonObject())) {
			value = json.getAsJsonObject().get(collumn).getAsString();
		}
		return value;
	}

	/**
	 * @author IcaroSilva
	 * @param json
	 * @return
	 */
	public static JsonArray getJsonArray(JsonObject json) {
		return  json.getAsJsonArray("resultset");
	}
	
	/**
	 * @author IcaroSilva
	 * @param jsonElement
	 * @param collumn
	 * @return
	 */
	public static String getValueJsonElement(JsonElement jsonElement, String collumn) {
		return jsonElement.getAsJsonObject().get(collumn.toLowerCase()).getAsString();
	}
}
