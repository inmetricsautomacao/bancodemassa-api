package br.com.inmetrics.bancodemassa.api.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static br.com.inmetrics.bancodemassa.api.utils.ConstantUtils.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MappingFromGherkin {
	public String key() default VAZIO;
	public String name() default VAZIO;
}
