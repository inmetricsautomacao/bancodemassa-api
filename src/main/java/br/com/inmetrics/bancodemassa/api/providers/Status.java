package br.com.inmetrics.bancodemassa.api.providers;

import static br.com.inmetrics.bancodemassa.api.utils.StringUtils.isNotNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Status {
	
	@Expose
	@SerializedName("cod_reserva")
	private String codigoDeReserva = null;
	
	@Expose
	@SerializedName("reuso")
	private String reuso = null;
	
	public Status(String codigoDeReserva, String reuso) {
		if (isNotNull(codigoDeReserva)) {
			this.codigoDeReserva = codigoDeReserva;
		}
		if (isNotNull(reuso)) {
			this.reuso = reuso;
		}
	}
	
	public String getCodigoDeReserva() {
		return codigoDeReserva;
	}
	
	public String getReuso() {
		return reuso;
	}

}
