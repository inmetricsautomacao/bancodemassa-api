package br.com.inmetrics.bancodemassa.api.providers;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReuseControl {
	
	@SerializedName("reuso")
	@Expose
	private int reuse = 0;
	
	public ReuseControl() {
	}

	public ReuseControl(int reuse) {
		this.reuse = reuse;
	}
	
	public int getReuse() {
		return reuse;
	}
	
}
