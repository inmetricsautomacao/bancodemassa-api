package br.com.inmetrics.bancodemassa.api.providers;

import static br.com.inmetrics.bancodemassa.api.utils.StringUtils.isNull;
import static br.com.inmetrics.bancodemassa.api.utils.StringUtils.postJson;

import java.lang.reflect.Type;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import br.com.inmetrics.bancodemassa.api.providers.config.Tag;

public class DataPost {
	
	@Expose
	@SerializedName(value="tags")
	private Tag tag = null;
	
	@Expose
	@SerializedName(value="massa")
	private Map <String, String> massa = null;
	
	public DataPost(Tag tag) {
		if (isNull(tag.getClasseDeMassa())) {
			throw new NullPointerException("Necessario informar pelo menos uma classe de massa");
		}
		this.tag = tag;
	}
	
	public DataPost incluir(Object json) {
		
		if (isNull(json)) {
			throw new NullPointerException("Necessario incluir pelo menos um criterio de massa");
		}
		
		if (json instanceof JsonObject) {
			Gson gson = new Gson();
			Type type = new TypeToken<Map<String, String>>(){}.getType();
			Map<String,String> map = gson.fromJson((JsonObject) json, type);
			this.massa = map;
			return this;
		}
		
		this.massa = postJson(json);
		return this;
	}
	
	public String getClasseDeMassa() {
		return this.tag.getClasseDeMassa();
	}
	
}
