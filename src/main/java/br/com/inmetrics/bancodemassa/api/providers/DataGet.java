package br.com.inmetrics.bancodemassa.api.providers;

import static br.com.inmetrics.bancodemassa.api.utils.StringUtils.isNotNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import br.com.inmetrics.bancodemassa.api.utils.MappingFromGherkin;

public class DataGet {
	
	@Expose
	@SerializedName(value="cod_reserva")
	private String codigoDeReserva = null;
	
	@Expose
	@SerializedName(value="classe_massa")
	private String classeDeMassa = null;
	
	@Expose
	@SerializedName(value="criterios_massa")
	private List<String> criteriosDeMassaAdicionais = null;
	
	@Expose
	@SerializedName(value="reuso")
	private String reuso = null;
	
	private List<String> criteriosTemp = null;
	
	public DataGet(String classeDaMassa) {
		if (isNotNull(classeDaMassa)) {
			this.classeDeMassa = classeDaMassa;
		} else {
			throw new NullPointerException("Necessario informar pelo menos uma classe de massa");
		}
	}
	
	public DataGet adicionarConfiguracaoDoCodigoDaReserva(String codigoDeReserva) {
		if (isNotNull(codigoDeReserva)) {
			this.codigoDeReserva = codigoDeReserva;
		}
		return this;
	}
	
	public DataGet adicionarConfiguracaoDoControleDeReuso(String reuso) {
		if (isNotNull(reuso)) {
			this.reuso = reuso;
		}
		return this;
	}
	
	public DataGet filtrar(Object obj) {
		
		this.criteriosTemp = new ArrayList<>();
		
		if (obj == null) {
			throw new NullPointerException("Critério não pode ser nulo");
		}
		
		Field[] fields = obj.getClass().getDeclaredFields();
		
		for (Field field : fields) {
			try {
				field.setAccessible(true);
				final String key = field.getAnnotation(MappingFromGherkin.class).key();
				if (field.isAnnotationPresent(MappingFromGherkin.class) && !key.isEmpty()) {
					String value = (String) field.get(obj);
					if (this.criteriosTemp.size() == 0) {
						criteriosTemp.add("json_massa->'massa'->");
						criteriosTemp.add("'" + key + "'" + ":" + "'" + value + "'");
					} else {
						criteriosTemp.add(" AND json_massa->'massa'->");
						criteriosTemp.add(key + ":" + value);
					}
				}
			} catch (IllegalAccessException e) {
				throw new IllegalAccessError("Erro ao montar o json para criterios");
			}
		}
		
		StringBuilder builder = new StringBuilder();
		this.criteriosTemp.forEach(criterio -> builder.append(criterio));
		
		this.criteriosDeMassaAdicionais = new ArrayList<>();
		this.criteriosDeMassaAdicionais.add(builder.toString());
		
		return this;
	}
	
	public String getCodigoDeReserva() {
		return codigoDeReserva;
	}

	public String getClasseDeMassa() {
		return classeDeMassa;
	}
	
	public List<String> getCriterios() {
		return criteriosDeMassaAdicionais;
	}
	
	public String getReuso() {
		return reuso;
	}

}
