package br.com.inmetrics.bancodemassa.api.template;

/**
 * Interface usada exclusivamente para definir quais operações serão usadas para um 'CRUD' no JSON
 * 
 * <p>Interface usada também para definir as templates de 'CRUD'</p>
 *
 * @author  Thiago Sakurai Paschoal
 * 
 * @since   1.0
 * 
 */
public interface JsonTemplate {
	
	String getJson();
	
	String postJson();
	
	String putJson();

}
