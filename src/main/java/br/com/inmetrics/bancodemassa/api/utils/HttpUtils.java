package br.com.inmetrics.bancodemassa.api.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

public final class HttpUtils {
	
	public static HttpHeaders createHeaders() {
		HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
        headers.add("Content-Type", MediaType.APPLICATION_JSON.toString());
		return headers;
	}

}
