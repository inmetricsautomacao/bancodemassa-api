package br.com.inmetrics.bancodemassa.api.providers.config;

import static br.com.inmetrics.bancodemassa.api.utils.StringUtils.isNotNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tag {
	
	@Expose
	@SerializedName(value="cod_reserva")
	private String codigoDeReserva = null;
	
	@Expose
	@SerializedName(value="classe_massa")
	private String classeDeMassa = null;
	
	@Expose
	@SerializedName(value="reuso")
	private String reuso = null;
	
	public Tag(String classeDeMassa) {
		this.classeDeMassa = classeDeMassa;
	}
	
	public Tag adicionarConfiguracaoDoCodigoDaReserva(String reservationCode) {
		if (isNotNull(reservationCode)) {
			this.codigoDeReserva = reservationCode;
		}
		return this;
	}
	
	public Tag includeReuseControl(String reuse) {
		if (isNotNull(reuse)) {
			this.reuso = reuse;
		}
		return this;
	}
	
	public String getClasseDeMassa() {
		return classeDeMassa;
	}
	
	public String getCodigoDeReserva() {
		return codigoDeReserva;
	}
	
	public String getReuso() {
		return reuso;
	}
	
}
