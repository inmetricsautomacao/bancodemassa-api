package br.com.inmetrics.bancodemassa.api.utils;

/**
 * Enum usado para inclusão dos ambientes usados na API
 * 
 * @author  Thiago Sakurai Paschoal
 * 
 * @see     Object
 * 
 * @since   2.0
 * 
 * @history 2.0 JosemarSilva Ambiente -> EnvironmentTypeUtils 
 * 
 */
public enum EnvironmentTypeUtils {
	
	PROD("PROD"), HML("HML"), TI("TI"), DEV("DEV");
	
	private String id;

	private EnvironmentTypeUtils(String id) {
		this.id = id;
	}
	
	public String buscarIdEnvironmentType(EnvironmentTypeUtils outroAmbiente) {
		for (EnvironmentTypeUtils ambiente : EnvironmentTypeUtils.values()) {
			if (ambiente.equals(outroAmbiente)) {
				return ambiente.getId();
			}
		}
		return "HML";
	}
	
	public String getId() {
		return id;
	}
	
}
