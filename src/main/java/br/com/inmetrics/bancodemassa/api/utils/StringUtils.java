package br.com.inmetrics.bancodemassa.api.utils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

/**
 * Classe, com metodos utilitarios para manipular strings
 * 
 * <p>StringUtils fornece:
 * <ul>
 * <li>Metodos para verificar se uma string e' nula ou vazia</li>
 * </ul>
 * 
 * @author Inmetrics
 *
 */
public final class StringUtils {

	static Logger logger = Logger.getLogger(StringUtils.class);

	/**
	 * Construtor privado
	 */
	private StringUtils() {
	}
	
	/**
	 * Formata Json de Request para o cantina, alterando a instrucaoo 'get'
	 * @param jsonSemFormatacao
	 * @return
	 * 
	 */
	public static String formataJsonGet(String jsonSemFormatacao) {
		return jsonSemFormatacao.replace("\"get\",", "\"get\":").replaceAll("[\\[\\]()]", "");
	}
	
	/**
	 * Formata Json de Request para o cantina, alterando a instrucaoo 'post'
	 * @param jsonSemFormatacao
	 * @return
	 * 
	 */
	public static String formataJsonPost(String jsonSemFormatacao) {
		return jsonSemFormatacao.replace("\"post\",", "\"post\":").replaceAll("[\\[\\]()]", "");
	}
	
	/**
	 * formataJsonPut() - Formata Json de Request para o cantina, alterando a instrucao 'put'
	 * 
	 * @param jsonSemFormatacao
	 * @return
	 * 
	 */
	public static String formataJsonPut(String jsonSemFormatacao) {
		return jsonSemFormatacao.replace("\"put\",", "\"put\":").replaceAll("[\\[\\]()]", "");
	}
	
	/**
	 * isEmptyOrNull()
	 * 
	 * @param param String String que deseja verificar se e' vazia
	 * @return se a String for nula ou vazia retorna true, caso contrario false.
	 * 
	 */
	public static boolean isEmptyOrNull(String param) {
		boolean ret = param != null && !param.isEmpty();
		return !ret;
	}
		
	/**
	 * hasValue()
	 * 
	 * @param value
	 * @return
	 */
	public static String hasValue(String value) {
		return isEmptyOrNull(value) ? "Nenhum" : value;
	}
	
	/**
	 * retornaProprioValor() - Se o valor for nulo ou vazio retorna vazio senao retorna ele mesmo
	 * @param value
	 * @return
	 */
	public static String retornaProprioValor(String value) {
		return isEmptyOrNull(value) ? ConstantUtils.VAZIO : value;
	}
	
	/**
	 * isNull() - Verifica se e null
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isNull(Object value) {
		return value == null;
	}

	/**
	 * isNotNull() - Valida se a object nao e null
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isNotNull(Object value) {
		return !isNull(value);
	}
	
	
	public static Map<String, String> postJson(Object obj) {
		Map<String, String> item = new HashMap<>();
		Field[] atributos = obj.getClass().getDeclaredFields();
		Arrays.asList(atributos).forEach(atributo -> {
			atributo.setAccessible(true);
			try {
				final String value = (String) atributo.get(obj);
				if (atributo.isAnnotationPresent(MappingFromGherkin.class)) {
					final String key = atributo.getAnnotation(MappingFromGherkin.class).key();
					if ((!isEmptyOrNull(key) && !isEmptyOrNull(value))) {
						item.put(key, value);
					}
				}
			} catch (IllegalAccessException e) {
				logger.error("Erro, ao criar a string de json", e);
			}
		});
		return item;
	}
	
		
}
