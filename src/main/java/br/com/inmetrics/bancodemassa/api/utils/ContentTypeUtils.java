package br.com.inmetrics.bancodemassa.api.utils;

/**
 * Enum usado para inclusão dos tipos de massa usados na API
 * 
 * @author  Thiago Sakurai Paschoal
 * 
 * @see     Object
 * 
 * @since   2.0
 * 
 * @history 2.0 JosemarSilva TipoMassa -> ContentTypeUtils 
 */
public enum ContentTypeUtils {
	
	ETL("ETL"), STAGE_PIPELINE("STAGE-PIPELINE");
	
	private String id;

	private ContentTypeUtils(String id) {
		this.id = id;
	}
	
	public String buscarIdContentType(ContentTypeUtils tipoDeMassa) {
		for (ContentTypeUtils tipo : ContentTypeUtils.values()) {
			if (tipo.equals(tipoDeMassa)) {
				return tipo.getId();
			}
		}
		return "ETL";
	}
	
	public String getId() {
		return id;
	}
	
}
