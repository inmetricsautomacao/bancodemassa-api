package br.com.inmetrics.bancodemassa.api.template.factorys;

import br.com.inmetrics.bancodemassa.api.providers.Node;
import br.com.inmetrics.bancodemassa.api.template.JsonTemplate;
import br.com.inmetrics.bancodemassa.api.template.bdmassa.ClusterManagerJson;

/**
 * Factory para criação apenas de um novo Gerenciador de Templates de Cluster.
 *
 * @author  Thiago Sakurai Paschoal
 * 
 * @see     Object
 * 
 * @see		JsonFactory
 * 
 * @since   1.0
 * 
 */
public class ClusterFactory implements JsonFactory {

	@Override
	public JsonTemplate createJson(Node nodeInput) {
		return new ClusterManagerJson(nodeInput);
	}

}
