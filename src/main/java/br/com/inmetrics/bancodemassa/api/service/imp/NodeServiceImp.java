package br.com.inmetrics.bancodemassa.api.service.imp;

import static br.com.inmetrics.bancodemassa.api.utils.JsonUtils.parserStringTOJson;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonObject;

import static br.com.inmetrics.bancodemassa.api.utils.StringUtils.isNull;
import static br.com.inmetrics.bancodemassa.api.utils.HttpUtils.createHeaders;

import br.com.inmetrics.bancodemassa.api.providers.Node;
import br.com.inmetrics.bancodemassa.api.service.NodeService;

/**
 * Classe que consiste apenas para gerenciar todas as operações possíveis para os nodes
 * 
 * @author ThiagoSakurai
 * 
 * @see NodeService
 * 
 * @see Object
 * 
 * @version 1.0.0
 *
 */
public class NodeServiceImp implements NodeService {
	
	private Logger logger = Logger.getLogger(NodeServiceImp.class);
	
	@Override
	public JsonObject buscar(Node node) {
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(buscarNodeDisponivel(node) + "/dados/{json}", String.class, node.getJson());
		return parserStringTOJson(result);
	}
	
	@Override
	public JsonObject incluir(Node node) {
		RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> entity = new HttpEntity<String>(node.postJson(), createHeaders());
        ResponseEntity<String> exchange = restTemplate.exchange(buscarNodeDisponivel(node) + "/incluirDados/json", POST, entity, String.class);
		return parserStringTOJson(exchange.getBody());
	}
	
	@Override
	public JsonObject alterarStatusDaMassa(Node node) {
		RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> entity = new HttpEntity<String>(node.putJson(), createHeaders());
        ResponseEntity<String> exchange = restTemplate.exchange(buscarNodeDisponivel(node) + "/atualizarStatus/json", PUT, entity, String.class);
		return parserStringTOJson(exchange.getBody());
	}

	private String buscarNodeDisponivel(Node node) {

		final String enderecoDoCluster = getEnderecoDoCluster();
		
		RestTemplate restTemplate = new RestTemplate();
		String result = restTemplate.getForObject(enderecoDoCluster + "{json}", String.class, node.getJsonDoCluster());

		if (isNull(result)) {
			final String msgError = "Não existe, o cluster que você deseja. Tente outros parâmetros";
			logger.error(msgError);
			throw new NullPointerException(msgError);
		}
		
		return parserStringTOJson(result).getAsJsonObject("node_return").get("url_rest_service").getAsString();
	}
	
	private String getEnderecoDoCluster() {
		try {
			Properties properties = new Properties();
			properties.load(getFileFromClassLoader("config.properties"));
			return properties.getProperty("endereco.cluster");
		} catch (IOException e) {
			logger.error("Erro, ao obter url do cluster", e);
			return null;
		}
	}
	
	private InputStream getFileFromClassLoader(String file) {
		try {
			ClassLoader classLoader = getClass().getClassLoader();
			URL resource = classLoader.getResource(file);
			return new FileInputStream(resource.getFile());
		} catch (Exception e) {
			return getClass().getClassLoader().getResourceAsStream(file);
		}
	}
}
