package br.com.inmetrics.bancodemassa.api.utils;

/**
 * 
 * Name: {@link Constants}
 * 
 * Proposito: Constantes comuns
 * 
 * @author Inmetrics
 * 
 * @see
 * 
 * @version 2.0
 * 
 * @history 2018-03-18 JosemarSilva Initilia Truncat
 *
 */
public final class ConstantUtils {

	/**
	 * Construtor privado
	 */
	private ConstantUtils() {
	}

	// GERAL
	public static final String VAZIO = "";
	
	public static final String APPLICATION_PROPERTIES = "/application.properties";

}
