# README #


## 1. Introdução ##

Este repositório contém o código fonte do componente **bancodemassa-api** da solução **Bancodemassa - Cantina**. O componente *bancodemassa-api* é um pacote .jar que deverá ser incluído no software aplicativo que deseja conectar-se a solução de *Bancodemassa Cantina*.

### 2. Documentação ###

### 2.1. Diagrama de Caso de Uso (Use Case Diagram) ###

```image-file
./doc/UseCaseDiagram.jpg
../../bancodemassa-doc/*
```

### 2.2. Diagrama de Implantação (Deploy Diagram) ###

```image-file
./doc/DeployDiagram.jpg
../../bancodemassa-doc/*
```

### 2.3. Diagrama Modelo Banco de Dados (Database Data Model) ###

* n/a

## 3. Projeto ##

### 3.1. Pré-requisitos ###

* Linguagem de programação: Java
* IDE: Eclipse
* JDK/JRE: 1.7 ou 1.8
* Postgresql 9.5+
* Postgresql database schemas ('bmcluster', 'bmconfig', 'bmnode') instalado e configurado
* Componentes *bancodemassa-ws-cluster* configurado e disponível
* Componentes *bancodemassa-ws-node* configurado e disponível
* Postgresql database initialization/configuration scripts executados

### 3.2. Guia para Desenvolvimento ###

* Obtenha o código fonte através de um "git clone". Utilize a branch "master" se a branch "develop" não estiver disponível.
* Faça suas alterações, commit e push na branch "develop".

### 3.3. Guia para Implantação ###

* Obtenha o último pacote (.jar) estável gerado disponível na sub-pasta './dist'.
* Configure os arquivos de parametrizações conforme o seu ambiente
* Inclua a dependência desta biblioteca ( bancodemassa-api.jar ) em sua aplicação.

### 3.4. Guia para Demonstração ###

* n/a

## Referências ##

* n/a
